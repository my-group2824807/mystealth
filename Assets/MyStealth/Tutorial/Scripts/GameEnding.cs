﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding: MonoBehaviour
{
	public float fadeDuration = 1f;
	public float displayImageDuration = 1f;
	public GameObject player;
	public CanvasGroup winBackgroundImageCanvasGroup;
	public AudioSource winAudio;
	public CanvasGroup loseBackgroundImageCanvasGroup;
	public AudioSource loseAudio;

	bool m_IsPlayerAtExit;
	bool m_IsPlayerCaught;
	float m_Timer;
	bool m_HasAudioPlayed;

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject == player)
		{
			m_IsPlayerAtExit = true;
		}
	}

	public void CaughtPlayer()
	{
		m_IsPlayerCaught = true;
	}

	void Update()
	{
		if (m_IsPlayerAtExit)
		{
			EndLevel(winBackgroundImageCanvasGroup, false, winAudio);
		}
		else if (m_IsPlayerCaught)
		{
			EndLevel(loseBackgroundImageCanvasGroup, true, loseAudio);
		}
	}

	void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
	{
		if (!m_HasAudioPlayed)
		{
			if (audioSource != null)
			{
				audioSource.Play();
			}
			m_HasAudioPlayed = true;
		}

		m_Timer += Time.deltaTime;
		imageCanvasGroup.alpha = m_Timer / fadeDuration;

		if (m_Timer > fadeDuration + displayImageDuration)
		{
			if (doRestart)
			{
				SceneManager.LoadScene(0);
			}
			else
			{
				Application.Quit();
			}
		}
	}
}
